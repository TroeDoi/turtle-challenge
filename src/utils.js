'use strict'

const fs = require('fs')

// Function to log results.
// If we are running tests, return the value instead of logging it
// For simplification purposes
const log = (value) => {
  if (process.env.NODE_ENV === 'test') {
    return value
  } else {
    console.log(value)
  }
}

// Read and return the content of settings files
const readJsonFiles = () => {
  const settingsDir = process.env.SETTINGS_DIR || `${__dirname}/../files`
  const gameSettingsFile = fs.readFileSync(`${settingsDir}/game_settings.json`)
  const movesFile = fs.readFileSync(`${settingsDir}/moves.json`)

  return {gameSettingsFile, movesFile}
}

module.exports = {
  log,
  readJsonFiles
}