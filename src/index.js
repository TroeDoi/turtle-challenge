'use strict'

const {get} = require('lodash')
const {log, readJsonFiles} = require('./utils')

let currentPosition, mines, exitX, exitY, boardWidth, boardHeight, moves

// Init the settings from the files:
const initSettings = () => {
  const {gameSettingsFile, movesFile} = readJsonFiles()

  const gameSettings = JSON.parse(gameSettingsFile)
  moves = JSON.parse(movesFile).moves

  // Board Size
  boardWidth = get(gameSettings, 'board_size.width')
  boardHeight = get(gameSettings, 'board_size.height')

  // Start Position
  const startX = get(gameSettings, 'start_position.x')
  const startY = get(gameSettings, 'start_position.y')
  const startDir = get(gameSettings, 'start_position.dir')

  // Exit Position
  exitX = get(gameSettings, 'exit_position.x')
  exitY = get(gameSettings, 'exit_position.y')

  // Array of mines' positions
  mines = get(gameSettings, 'mines')

  // Position of the turtle
  currentPosition = {
    x: startX,
    y: startY,
    dir: startDir
  }
}

// Updates currentPosition doing a movement in the current direction:
const doMovement = () => {
  const currentDir = currentPosition.dir
  switch (currentDir) {
    case 'North':
      currentPosition.y = currentPosition.y - 1
      break
    case 'South':
      currentPosition.y = currentPosition.y + 1
      break
    case 'East':
      currentPosition.x = currentPosition.x + 1
      break
    case 'West':
      currentPosition.x = currentPosition.x - 1
      break
  }
}

// Updates currentPosition rotating 90 degrees to the right:
const doRotation = () => {
  const currentDir = currentPosition.dir
  switch (currentDir) {
    case 'North':
      currentPosition.dir = 'East'
      break
    case 'East':
      currentPosition.dir = 'South'
      break
    case 'South':
      currentPosition.dir = 'West'
      break
    case 'West':
      currentPosition.dir = 'North'
      break
  }
}

// Returns true if there is a mine in the current position:
const isMineHit = () => {
  const currentX = currentPosition.x
  const currentY = currentPosition.y
  for (let x = 0; x < mines.length; x++) {
    const mine = mines[x]
    if (mine.x === currentX && mine.y === currentY) {
      return true
    }
  }
  return false
}

// Returns the results of the current position:
const logResult = () => {
  if (currentPosition.x === exitX && currentPosition.y === exitY) {
    return log('Success.')
  } else
  if (currentPosition.x > boardWidth - 1
    || currentPosition.x < 0
    || currentPosition.y > boardHeight - 1
    || currentPosition.y < 0) {
    return log('Out of bounds.')
  } else {
    return log('Still in danger.')
  }
}

// Starts the game
const startGame = () => {
  initSettings()
  let isMine = false
  for (let i = 0; i < moves.length; i++) {
    const moveType = moves[i]
    if (moveType === 'm') {
      doMovement()
    } else if (moveType === 'r') {
      doRotation()
    }
    isMine = isMineHit()
    // If the turtle hits a mine, stop the game and returns the result
    if (isMine) {
      return log('Mine Hit!')
    }
  }
  return logResult()
}

// Trigger startGame when not running tests:
if (process.env.NODE_ENV !== 'test') {
  startGame()
}

// Export the function for tests purposes:
module.exports = startGame