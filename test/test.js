const assert = require('assert')
const startGame = require('../src')

describe('Turtle Game', function() {
  it('Should return Success', function() {
    process.env.SETTINGS_DIR = `${__dirname}/success_files`
    const r = startGame()
    assert.equal(r, 'Success.')
  })

  it('Should return Mine Hit!', function() {
    process.env.SETTINGS_DIR = `${__dirname}/mine_hit_files`
    const r = startGame()
    assert.equal(r, 'Mine Hit!')
  })

  it('Should return Out of bounds', function() {
    process.env.SETTINGS_DIR = `${__dirname}/out_of_bounds_files`
    const r = startGame()
    assert.equal(r, 'Out of bounds.')
  })

  it('Should return Still in danger', function() {
    process.env.SETTINGS_DIR = `${__dirname}/still_in_danger_files`
    const r = startGame()
    assert.equal(r, 'Still in danger.')
  })
})