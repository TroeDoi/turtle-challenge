# Turtle Challenge

Simple turtle game.

## Requirements

You will only need Node.js and npm installed in your environment.
I used the LTS version of node (v10.16.0) and npm (v6.9.0)

## Install

    git clone https://gitlab.com/TroeDoi/turtle-challenge.git
    cd turtle-challenge
    npm install    

## Running the project
By default, it will use the game settings and moves from the folder `/files`. You can specify a different folder:
```
export SETTINGS_DIR=test/still_in_danger_files
```
To run the game with the specified folder:
```
npm start
```
To run the tests (it covers all the possibles endings):
```
npm test
```

## Notes
For simplicity in this exercise, I used the same game_settings for all the cases, the moves file is different to achieve all the possibles endings.
